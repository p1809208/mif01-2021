package fr.univ_lyon1.info.m1.cv_search.model.data;


import java.util.List;

/**
 *  This class is just used for the display of applicants for the result table.
 */
public class ApplicantTab {
    private int average;
    private String name;
    private List<ExperienceTab> listExperiences;

    /**
     *
     * @param name name of applicant
     * @param average average of applicant
     * @param listExps list Experiences of applicant
     */
    public ApplicantTab(String name, int average, List<ExperienceTab> listExps) {
        this.name = name;
        this.average = average;
        this.listExperiences = listExps;
    }

    public List<ExperienceTab> getListExps() {
        return listExperiences;
    }

    public void setListExps(List<ExperienceTab> listExps) {
        this.listExperiences = listExps;
    }

    public List<ExperienceTab> getListExperiences() {
        return listExperiences;
    }

    public void setListExperiences(List<ExperienceTab> listExperiences) {
        this.listExperiences = listExperiences;
    }

    public Integer getAverage() {
        return average;
    }

    public void setAverage(Integer average) {
        this.average = average;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
