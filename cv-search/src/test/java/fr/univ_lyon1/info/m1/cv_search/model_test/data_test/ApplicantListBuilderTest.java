package fr.univ_lyon1.info.m1.cv_search.model_test.data_test;

import java.io.File;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;

public class ApplicantListBuilderTest {
    @Test
    public void testApplicantListBuilder()
    {
        //When
        ApplicantListBuilder builderList = new ApplicantListBuilder(new File("./"));

        //Given
        ApplicantList applicants = builderList.build();

        //Then
        assertThat(5, is(applicants.size()));
        
    } 
}
