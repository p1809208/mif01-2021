package fr.univ_lyon1.info.m1.cv_search.model_test.metier_test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import fr.univ_lyon1.info.m1.cv_search.model.metier.Calcul;
import fr.univ_lyon1.info.m1.cv_search.model.metier.CalculAverage;
//import fr.univ_lyon1.info.m1.cv_search.model.metier.CalculAverage;
import fr.univ_lyon1.info.m1.cv_search.model.metier.Tri;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
//import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
//import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;




public class TriTest {

    @Test
    public void testTri() {

        
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("./applicant4.yaml");
        ApplicantBuilder builder2 = new ApplicantBuilder("./applicant2.yaml"); 
        ApplicantBuilder builder3 = new ApplicantBuilder("./applicant5.yaml");
        ApplicantBuilder builder4 = new ApplicantBuilder("./applicant1.yaml");  

        // When
        Applicant a = builder.build();
        Applicant a2 = builder2.build();
        Applicant a3 = builder3.build();
        Applicant a4 = builder4.build();
        List<String>  listSkills = new ArrayList<>();
        listSkills.add("c");
        listSkills.add("c++");
        listSkills.add("java");
        Calcul calcul = new CalculAverage();
        a.setAverage(calcul.compute(a, listSkills));
        a2.setAverage(calcul.compute(a2, listSkills));
        a3.setAverage(calcul.compute(a2, listSkills));
        a4.setAverage(calcul.compute(a2, listSkills));
        List<Applicant> l = new ArrayList<>();
        List<Applicant> lTriee = new ArrayList<>();
        l.add(a);
        l.add(a2);
        l.add(a3);
        l.add(a4);        
        Tri.triList(l);

        // Then
        assertThat("Foo Bar", is(l.get(0).getName()));
        assertThat("Tata Tata", is(l.get(1).getName()));
        assertThat("John Smith", is(l.get(2).getName()));
        assertThat("Titi Titi", is(l.get(3).getName()));

    }
}