package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.view.JfxView;

public interface Notify {
    /**
     *
     * @param view instance of view
     */
    void notify(JfxView view);
}
