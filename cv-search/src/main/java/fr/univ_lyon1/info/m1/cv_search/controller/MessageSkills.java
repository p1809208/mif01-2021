package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.view.JfxView;

import java.util.List;

public class MessageSkills implements Notify {
    private Message message;
    private String skillName;
    private List<String> skillList;

    /**
     *
     * @param message message of notification
     * @param skillName name of skill
     * @param skillList list of skills
     */
    MessageSkills(Message message, String skillName, List<String> skillList) {
        this.message = message;
        this.skillName = skillName;
        this.skillList = skillList;
    }

    /**
     *
     * @param view view instance of view
     */
    @Override
    public void notify(JfxView view) {
        if (message == Message.AddSkill) {
            addSkill(skillList);
        } else {
            removeSkill(skillList);
        }
        view.updateButtonSkill(skillName, message);

    }

    /**
     *
     * @param skillList list of skills
     */
    void addSkill(List<String> skillList) {
        skillList.add(skillName);
    }

    /**
     *
     * @param skillList list of skills
     */
    void removeSkill(List<String> skillList) {
        skillList.remove(skillName);
    }

}
