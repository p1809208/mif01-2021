package fr.univ_lyon1.info.m1.cv_search.controller;

public enum Message {
    AddSkill,
    RemoveSkill,
    AddResetButton,
    ResetSearch,
    ActivateAdvancedSearch,
    DisableAdvancedSearch,
    NoSelectStrategy,
    NoChoisSkill,
    NoChoisCompany,
    NoChoisDuration,
    deleteMsgErrorCompany,
    deleteMsgErrorDuration;
}

