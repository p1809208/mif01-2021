package fr.univ_lyon1.info.m1.cv_search.model.metier;

public enum TypeStrategy {
        Default("All >= 50%"),
        DefaultPlus("All >= 60%"),
        Average("Average >= 50%"),
        Experience("Experience > 1 ans"),
        AdvancedSearch("Advanced Search");

        private final String strategy;

        TypeStrategy(String valeur) {
            strategy = valeur;
        }

        public String valeur() {
            return  strategy;
        }

        /**
         *
         * @param value value of strategy in String
         * @return value of strategy in TypeStrategy
         */

        public static TypeStrategy getStrategy(String value) {
                switch (value) {
                        case "All >= 50%" :
                                return Default;
                        case "All >= 60%" :
                                return DefaultPlus;
                        case "Average >= 50%" :
                                return  Average;
                        case  "Experience > 1 ans" :
                                return Experience;
                        case "Advanced Search":
                                return AdvancedSearch;
                        default:
                                return null;
                }
        }


}

