package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;

import java.util.List;

public class AdvancedSearchAdaptateur implements Strategy {
    private  String durationExperience;
    private  String companyName;

    public AdvancedSearchAdaptateur() {
        System.out.println("Creation une instance de RechercheExperienceAdaptateur");

    }
    @Override
    public List<Applicant> strategySearch(ApplicantList listApplicants,
      List<String> listSkills) {
      return  AdvancedSearch.searchAdvanced(
              listApplicants, companyName,
              durationExperience, listSkills);
    }



    public String getDurationExperience() {
        return durationExperience;
    }

    public String getCompanyName() {
        return companyName;
    }


    public void setDurationExperience(String durationExperience) {
        this.durationExperience = durationExperience;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
