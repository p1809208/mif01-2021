package fr.univ_lyon1.info.m1.cv_search.model_test.metier_test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import fr.univ_lyon1.info.m1.cv_search.model.metier.AdvancedSearch;
import fr.univ_lyon1.info.m1.cv_search.model.metier.Calcul;
import fr.univ_lyon1.info.m1.cv_search.model.metier.CalculAverage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;

public class AdvancedSearchTest {
    @Test
    public void testAdvancedSearch() {
        //Given 
        ApplicantListBuilder builderList = new ApplicantListBuilder(new File("./"));
        Calcul calculAverage = new CalculAverage();
        List<Applicant>  resultCalculAverageList1 = new ArrayList<>();
        List<Applicant>  resultCalculAverage1List1 = new ArrayList<>();
        /*List<Applicant>  resultCalculAverageList2 = new ArrayList<>(); */

        //When
        ApplicantList applicants = builderList.build();
        ApplicantList applicants2 = builderList.build();
        List<String>  listSkill1 = new ArrayList<>();
        //List<String>  listSkill2 = new ArrayList<>();
        listSkill1.add("c");
        /* listSkill2.add("c");
        listSkill2.add("c++");
        listSkill2.add("java"); */
    
        resultCalculAverageList1 = AdvancedSearch.searchAdvanced(applicants, "Google", "4", listSkill1);
        //resultCalculAverage1List1 = AdvancedSearch.searchAdvanced(applicants, "UCBL", "10", listSkill1, calculAverage);
        //resultCalculAverageList2 = AdvancedSearch.searchAdvanced(applicants2, "Google", "5", listSkill2, calculAverage);
        

        // Then
        //Test for "Google" experience, duration 5, list with skill "c"
        assertThat("Tata Tata", is(resultCalculAverageList1.get(0).getName()));
        assertThat("Titi Titi", is(resultCalculAverageList1.get(1).getName()));
        assertThat("Toto Toto", is(resultCalculAverageList1.get(2).getName()));
        assertEquals(3, resultCalculAverageList1.size());
        //Test fo "Facebook" experience , duration 17, list with skills "c", "c++", "java"
        //assertThat("Foo Bar", is(resultCalculAverage1List1.get(0).getName())); 
        //assertEquals(1,resultCalculAverage1List1.size());
 
        //Test for "Google" experience, duration 5, list with skills "c", "c++", "java"
        /* assertThat(listA3.get(0).getName(), is(resultCalculAverageList2.get(0).getName()));
        assertThat(listA3.get(1).getName(), is(resultCalculAverageList2.get(1).getName()));
        assertEquals(2, resultCalculAverageList1.size()); */
    }
}