package fr.univ_lyon1.info.m1.cv_search.model.data;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;


public class Applicant {
    private final Map<String, Integer> skills = new HashMap<>();
    private final Map<String, Experience> experiences = new HashMap<>();
    private String name;
    private Integer average;

    /**
     *
     * @param string key of experience
     * @param value value of experience
     */
    public void setExperiences(String string, Experience value) {
        experiences.put(string, value);
    }

    /**
     *
     * @return experiences;
     */
    public Map<String, Experience> getExperiences() {
        return experiences;
    }

    /**
     *
     * @return experiences.keySet();
     */
    public Set<String> getExperiencesKey() {
        return experiences.keySet();
    }

    /**
     *
     * @param string value of skill
     * @return add skill
     */
    public int getSkill(String string) {
        return skills.getOrDefault(string, 0);
    }

    /**
     *
     * @param string experience key
     * @return add experience
     */
    public Experience getExperience(String string) {
        return experiences.getOrDefault(string, null);
    }

    /**
     *
     * @return average of applicant
     */

    public Integer getAverage() {
        return average;
    }

    /**
     *
     * @param average new average of applicant
     */
    public void setAverage(int average) {
        this.average = average;
    }

    /**
     *
     * @param string skill name
     * @param value skill score
     */
    public void setSkill(String string, int value) {
        skills.put(string, value);
    }

    /**
     *
     * @return Applicant name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name name of applicant
     */
    public void setName(String name) {
        this.name = name;
    }
}
