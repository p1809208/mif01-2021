package fr.univ_lyon1.info.m1.cv_search.model_test.data_test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;

public class ApplicantListTest {
    @Test
    public void testApplicantListTest(){
        // Given
        ApplicantBuilder builderApplicant1 = new ApplicantBuilder("./applicant1.yaml");
        ApplicantBuilder builderApplicant2 = new ApplicantBuilder("./applicant2.yaml");
        ApplicantBuilder builderApplicant3 = new ApplicantBuilder("./applicant3.yaml");
        ApplicantBuilder builderApplicant4 = new ApplicantBuilder("./applicant4.yaml");
        ApplicantBuilder builderApplicant5 = new ApplicantBuilder("./applicant5.yaml");

        // When
        Applicant a1 = builderApplicant1.build();
        Applicant a2 = builderApplicant2.build();
        Applicant a3 = builderApplicant3.build();
        Applicant a4 = builderApplicant4.build();
        Applicant a5 = builderApplicant5.build();
        ApplicantList listA = new ApplicantList();
        listA.add(a1);
        listA.add(a2);
        listA.add(a3);
        listA.add(a4);
        listA.add(a5);

        //given
        assertEquals(5, listA.size());
    }
}
