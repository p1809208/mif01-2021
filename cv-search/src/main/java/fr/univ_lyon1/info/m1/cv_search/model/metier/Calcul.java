package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import java.util.List;

public interface Calcul {
     /**
      *
      * @param applicant applicant
      * @param listSkills list skills selected
      * @return value calculate
      */
     int compute(Applicant applicant, List<String> listSkills);
}
