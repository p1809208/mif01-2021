package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.Experience;

import java.util.ArrayList;
import java.util.List;

public class StrategyExperience implements Strategy {

    public StrategyExperience() {
        System.out.println("Creation une instance de StrategyExperience");
    }
    @Override
    public List<Applicant> strategySearch(ApplicantList listApplicants,
                                          List<String> listSkills) {
        Calcul calcul = new CalculAverage();
        List<Applicant> listTrier = new ArrayList<>();
        for (Applicant a : listApplicants) {
            int compteurSkill = 0;
            for (String skillSelect : listSkills) {
                for (String exp : a.getExperiencesKey()) {
                    Experience experience = a.getExperiences().get(exp);
                    List<String> skillsExperience = experience.getKeywords();
                    //skillAcquired
                    for (String skillAcquired : skillsExperience) {
                        if ((skillSelect.equals(skillAcquired))) {
                            int difference = (experience.getEndExperience())
                                    - (experience.getStartExperience());
                            if (difference > 1) {
                                compteurSkill++;
                            }

                        }

                    }
                }
            }
            if (compteurSkill > 1) {
                a.setAverage(calcul.compute(a, listSkills));
                listTrier.add(a);
            }
        }

        Tri.triList(listTrier);

        return new ArrayList<>(listTrier);
    }
}
