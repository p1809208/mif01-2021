package fr.univ_lyon1.info.m1.cv_search.model.data;


import java.util.ArrayList;
import java.util.List;

public class Experience {
    private String entreprise;
    private Integer startExperience;
    private Integer endExperience;
    private List<String> keywords;


    /**
     *
     * @param entreprise entreprise name
     * @param startExperience date of start experience
     * @param endExperience date of end experience
     * @param keywords keywords of experience
     */
    Experience(String entreprise, Integer startExperience,
               Integer endExperience, List<String> keywords) {
        this.entreprise = entreprise;
        this.startExperience = startExperience;
        this.endExperience = endExperience;
        this.keywords = keywords;
    }


    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {

        this.entreprise = entreprise;

    } 

    public Integer getStartExperience() {
        return startExperience;
    }

    public void setStartExperience(Integer startExperience) {
        this.startExperience = startExperience;
    }

    public Integer getEndExperience() {
        return endExperience;
    }

    public void setEndExperience(Integer endExperience) {
        this.endExperience = endExperience;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(ArrayList<String> keywords) {
        this.keywords = keywords;
    }
}
