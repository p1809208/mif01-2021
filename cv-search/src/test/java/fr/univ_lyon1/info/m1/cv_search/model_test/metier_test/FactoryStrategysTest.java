package fr.univ_lyon1.info.m1.cv_search.model_test.metier_test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.metier.FactoryStrategies;
import fr.univ_lyon1.info.m1.cv_search.model.metier.TypeStrategy;

public class FactoryStrategysTest {
    @Test
    public void testFactoryStrategys() {
        //Given
        ApplicantListBuilder builderList = new ApplicantListBuilder(new File("./"));
        ApplicantBuilder builder = new ApplicantBuilder("./applicant1.yaml");
        TypeStrategy strategyT= TypeStrategy.Default;

        //FactoryStrategys factoryS = new FactoryStrategys();
        List<String> list = new ArrayList<>();
        List<Applicant> listA = new ArrayList<>();
        List<Applicant> listResult = new ArrayList<>();
        list.add("c");
        list.add("c++");
        list.add("java");

        //When
        ApplicantList applicants = builderList.build();
        Applicant a = builder.build();
        listA = FactoryStrategies.makeStrategy(strategyT , applicants, list);
        listResult.add(a);

        //Then
        assertThat(listResult.get(0).getName(),is(listA.get(0).getName()));

    }
}
