package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;

import java.util.List;

public class CalculSomme implements Calcul {
    @Override
    public int compute(Applicant applicant, List<String> listSkills) {
        int scoreSkill = 0;
        for (String skill : listSkills) {
            scoreSkill += applicant.getSkill(skill);
        }
        return scoreSkill;
    }
}
