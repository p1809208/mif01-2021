package fr.univ_lyon1.info.m1.cv_search.controller_test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.controller.Message;
import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.Experience;
import fr.univ_lyon1.info.m1.cv_search.model.metier.FactoryStrategies;
import fr.univ_lyon1.info.m1.cv_search.model.metier.TypeStrategy;
import fr.univ_lyon1.info.m1.cv_search.view.JfxView;
import javafx.stage.Stage;

public class ControllerTest {
    private Controller controller = new Controller();

    @Before
    public void setUp() {
        controller = new Controller();
    }

    @Test
    public void testRunDefaultStrategy(){
        // Given
//        ApplicantListBuilder builderList = new ApplicantListBuilder(new File("./"));
//        ApplicantBuilder builder1 = new ApplicantBuilder("./applicant1.yaml");

        // When
       // Applicant a1 = builder1.build();
        List<Applicant> list = new ArrayList<Applicant>();
        controller.getSkillList().add("c");
        controller.getSkillList().add("c++");
        controller.getSkillList().add("java");
        controller.run(TypeStrategy.Default);
        list = controller.getResults();

        //Then
        assertThat("John Smith", is(list.get(0).getName()));
        assertThat("Tata Tata", is(list.get(1).getName()));
        assertThat("Foo Bar", is(list.get(2).getName()));
        assertThat("Titi Titi", is(list.get(3).getName()));
        assertThat(4, is(list.size()));
}
    @Test
    public void testRunDefaultPlusStrategy(){
        // Given

        // When
        List<Applicant> list = new ArrayList<Applicant>();
        controller.getSkillList().add("c");
        controller.run(TypeStrategy.DefaultPlus);
        list = controller.getResults();

        //Then
        assertThat("John Smith", is(list.get(0).getName()));
        assertThat("Tata Tata", is(list.get(1).getName()));
        assertThat(2, is(list.size()));
}
    @Test
    public void testRunAverageStrategy(){
        // Given

        // When

        List<Applicant> list = new ArrayList<Applicant>();
        controller.getSkillList().add("c");
        controller.getSkillList().add("java");
        controller.getSkillList().add("c++");
        controller.run(TypeStrategy.Average);
        list = controller.getResults();

        //Then
        assertThat("John Smith", is(list.get(0).getName()));
        assertThat("Tata Tata", is(list.get(1).getName()));
        assertThat("Foo Bar", is(list.get(2).getName()));
        assertThat("Titi Titi", is(list.get(3).getName()));
        assertThat(4, is(list.size()));
}

    @Test
    public void testRunExperienceStrategy(){
    // Given

    // When
    List<Applicant> list = new ArrayList<Applicant>();
    controller.getSkillList().add("c");
    controller.getSkillList().add("c++");
    controller.getSkillList().add("java");
    controller.run(TypeStrategy.Experience);
    list = controller.getResults();

    //Then
    assertThat("John Smith", is(list.get(0).getName()));
    assertThat("Tata Tata", is(list.get(1).getName()));
    assertThat("Foo Bar", is(list.get(2).getName()));
    assertThat("Titi Titi", is(list.get(3).getName()));
    assertThat("Toto Toto", is(list.get(4).getName()));
    assertThat(5, is(list.size()));
}

    @Test
    public void testRunAdvancedSearchStrategy(){
    // Given

    // When
    List<Applicant> list = new ArrayList<Applicant>();
    controller.getSkillList().add("c");
    controller.notifySearchWithAdvancedSearch("Google", "4");
    controller.run(TypeStrategy.AdvancedSearch);
    list = controller.getResults();

    //Then
    assertThat("Tata Tata", is(list.get(0).getName()));
    assertThat("Titi Titi", is(list.get(1).getName()));
    assertThat("Toto Toto", is(list.get(2).getName()));
    assertThat(3, is(list.size()));

}

}
