package fr.univ_lyon1.info.m1.cv_search.model_test.data_test;

import java.io.File;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;

public class ApplicantBuilderTest {
    @Test
    public void testApplicantBuilder(){
        // Given
        ApplicantBuilder builderApplicant1 = new ApplicantBuilder("./applicant1.yaml");
        ApplicantBuilder builderApplicant2 = new ApplicantBuilder("./applicant2.yaml");
        //Applicant3 whithout experiences
        //ApplicantBuilder builderApplicant3 = new ApplicantBuilder("./cv/applicant3.yaml");
        ApplicantBuilder builderApplicant4 = new ApplicantBuilder("./applicant4.yaml");
        String f ="./applicant5.yaml";
        File applicantFile = new File(f);
        ApplicantBuilder builderApplicant5 = new ApplicantBuilder(applicantFile);

        // When
        Applicant a1 = builderApplicant1.build();
        Applicant a2 = builderApplicant2.build();
        //Applicant a3 = builderApplicant3.build();
        Applicant a4 = builderApplicant4.build();
        Applicant a5 = builderApplicant5.build();

        //Then
        assertThat(a1.getName(), is("John Smith"));
        assertThat(a1.getSkill("c"), is(90));
        assertThat(a2.getName(), is("Foo Bar"));
        assertThat(a2.getSkill("c"), is(50));
        //assertThat(a3.getName(), is("Toto Toto"));
        assertThat(a4.getName(), is("Titi Titi"));
        assertThat(a4.getSkill("c"), is(50));
        assertThat(a5.getName(), is("Tata Tata"));
        assertThat(a5.getSkill("c"), is(60));
    }
}
