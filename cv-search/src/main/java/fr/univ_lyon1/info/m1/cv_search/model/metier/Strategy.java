package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;

import java.util.List;


public interface Strategy {
    /**
     *
     * @param listApplicants list of applicants
     * @param listSkills list of skills selected
     * @return selected candidates sorted by their average
     */
    List<Applicant> strategySearch(ApplicantList listApplicants,
                                   List<String> listSkills);

}
