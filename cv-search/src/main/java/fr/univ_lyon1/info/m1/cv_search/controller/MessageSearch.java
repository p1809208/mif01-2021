package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.view.JfxView;



public class MessageSearch implements Notify {
    private final Message message;

    MessageSearch(Message message) {
        this.message = message;
    }

    /**
     *
     * @param view view instance of view
     */
    @Override
    public void notify(JfxView view) {
        view.updateSearch(message);
    }
}
