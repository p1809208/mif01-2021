package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;

import java.util.ArrayList;
import java.util.List;

public class StrategyDefault implements Strategy {
    public StrategyDefault() {
        System.out.println("Creation une instance de StrategyDefault");
    }

    @Override
    public List<Applicant> strategySearch(ApplicantList listApplicants,
                                          List<String> listSkills) {
        Calcul calcul = new CalculAverage();
        List<Applicant> listTrier = new ArrayList<>();
        for (Applicant a : listApplicants) {
            boolean selected = true;
            for (String skill : listSkills) {
                if (a.getSkill(skill) < 50) {
                    selected = false;
                    break;
                }
            }
                if (selected) {
                    a.setAverage(calcul.compute(a, listSkills));
                    listTrier.add(a);
                }
            }

            Tri.triList(listTrier);

        return new ArrayList<>(listTrier);
            }
        }


