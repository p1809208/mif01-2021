package fr.univ_lyon1.info.m1.cv_search.model_test.data_test;

import static org.junit.Assert.*;

import fr.univ_lyon1.info.m1.cv_search.model.metier.CalculAverage;

import static org.hamcrest.CoreMatchers.is;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantTab;
import fr.univ_lyon1.info.m1.cv_search.model.data.Experience;
import fr.univ_lyon1.info.m1.cv_search.model.data.ExperienceTab;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;

public class ApplicantTabTest {
    @Test
    public void testApplicantTab() {
        
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("./applicant1.yaml");
        CalculAverage calcul = new CalculAverage();

        // When
        Applicant a = builder.build();
        ArrayList<String> key = new ArrayList<>(a.getExperiences().keySet());
        List<Experience> valuesList = new ArrayList<>(a.getExperiences().values());
        List<String> keywordsList = new ArrayList<>();
        List<String> keywordsList2 = new ArrayList<>();
        keywordsList.add("c");
        keywordsList.add("c++");
        keywordsList.add("java");
        keywordsList2.add("Python");
        keywordsList2.add("PHP");
        keywordsList2.add("c");
       
        int average = calcul.compute(a, keywordsList);
        List<ExperienceTab> listExps1=new ArrayList<>();
        List<ExperienceTab> listExps2=new ArrayList<>();
        ExperienceTab eTExp1 = new ExperienceTab(key.get(0), valuesList.get(0).getStartExperience(), valuesList.get(0).getEndExperience(), valuesList.get(0).getKeywords());
        ExperienceTab eTExp2 = new ExperienceTab(key.get(1), valuesList.get(1).getStartExperience(), valuesList.get(1).getEndExperience(), valuesList.get(1).getKeywords());
        listExps1.add(eTExp1);
        listExps2.add(eTExp2);
        ApplicantTab aT1= new ApplicantTab(a.getName(), average, listExps1);
        ApplicantTab aT2= new ApplicantTab(a.getName(), average, listExps2);
    
        // Then
        String n = aT1.getName();
        int avg =aT1.getAverage();
        String n2 = aT2.getName();
        int avg2 =aT2.getAverage();
        List<ExperienceTab> listE = aT1.getListExps();
        List<ExperienceTab> listE2 = aT2.getListExps();
        

        ExperienceTab tList = new ExperienceTab("Google", 2005, 2010, keywordsList);
        ExperienceTab tList2 = new ExperienceTab("Facebook", 2010, 2021, keywordsList2);  

        //When
        assertEquals(70, avg);
        assertThat("John Smith", is(n));
        //Test first experience of applicant1
        assertEquals(tList.getEntreprise(),  listE.get(0).getEntreprise()); 
        assertEquals(tList.getStartExperience(),  listE.get(0).getStartExperience());
        assertEquals(tList.getEndExperience(),  listE.get(0).getEndExperience()); 
        assertEquals(tList.getKeywords(),  listE.get(0).getKeywords());
        //Test second experience of applicant1
        assertEquals(tList2.getEntreprise(),  listE2.get(0).getEntreprise());
        assertEquals(tList2.getStartExperience(),  listE2.get(0).getStartExperience());
        assertEquals(tList2.getEndExperience(),  listE2.get(0).getEndExperience()); 
        assertEquals(tList2.getKeywords(),  listE2.get(0).getKeywords());
    }
}