package fr.univ_lyon1.info.m1.cv_search.model.data;

import java.util.List;

/**
 * This class is just used for the display of experiences for the result table.
 */
public class ExperienceTab {
    private String entreprise;
    private Integer startExperience;
    private Integer endExperience;
    private List<String> keywords;

    /**
     *
     * @param entreprise entreprise name
     * @param startExperience date of start experience
     * @param endExperience date of end experience
     * @param keywords keywords of experience
     */
     public ExperienceTab(String entreprise, Integer startExperience,
                          Integer endExperience, List<String> keywords) {
         this.entreprise = entreprise;
         this.startExperience = startExperience;
         this.endExperience = endExperience;
         this.keywords = keywords;
     }
    public String getEntreprise() {
        return entreprise;
    }

    public Integer getStartExperience() {
        return startExperience;
    }

    public Integer getEndExperience() {
        return endExperience;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public void setStartExperience(Integer startExperience) {
        this.startExperience = startExperience;
    }

    public void setEndExperience(Integer endExperience) {
        this.endExperience = endExperience;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public String toString() {
        return   '\n' + "{" + '\n'
                + "entreprise : '" + entreprise + '\n'
                + ", startExperience : " + startExperience + '\n'
                + ", endExperience : " + endExperience + '\n'
                + ", keywords : " + keywords + '\n'
                + '}' + '\n';
    }
}
