package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;


import java.util.Comparator;
import java.util.List;

public class Tri {
    protected Tri() {
        System.out.println("Creation une instance de Tri");
    }


    static final Comparator<Applicant> ORDER_MOYENNE =
            new Comparator<Applicant>() {
                public int compare(Applicant d1, Applicant d2) {
                    return d2.getAverage().compareTo(d1.getAverage());
                }
            };

    /**
     *
     * @param listApplicant sorted candidates by their average
     */
    public static void triList(List<Applicant> listApplicant) {
            listApplicant.sort(ORDER_MOYENNE);
        }

}

