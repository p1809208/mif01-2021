package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.view.JfxView;

public class MessageAdvancedSearch implements Notify {
    private final Message message;

    MessageAdvancedSearch(Message message) {
        this.message = message;
    }

    /**
     *
     * @param view instance of view
     */
    @Override
    public void notify(JfxView view) {
        view.updateAdvancedSearch(message);

    }

}

