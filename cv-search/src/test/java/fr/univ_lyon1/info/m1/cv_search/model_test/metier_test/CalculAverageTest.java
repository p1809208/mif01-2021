package fr.univ_lyon1.info.m1.cv_search.model_test.metier_test;

import static org.junit.Assert.*;

import fr.univ_lyon1.info.m1.cv_search.model.metier.CalculAverage;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;

public class CalculAverageTest {

    @Test
    public void testCalculAverage() {
        
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("./applicant1.yaml");
        CalculAverage calcul = new CalculAverage();

        // When
        Applicant a = builder.build();
        List<String>  listSkill = new ArrayList<>();
        listSkill.add("c");
        listSkill.add("c++");
        listSkill.add("java");
        int average = calcul.compute(a, listSkill);

        // Then
        assertEquals(70, average);

    }
}