package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;

import java.util.List;


public final class FactoryStrategies {
    private FactoryStrategies() {

    }
    private static AdvancedSearchAdaptateur advancedSearchAdaptateur;

    /**
     * @param typeStrategy type of strategy
     * @param applicantList  list of applicants
     * @param listSkills list of skills
     * @return result of strategy
     */
    public static List<Applicant> makeStrategy(
            TypeStrategy typeStrategy,
            ApplicantList applicantList,
            List<String> listSkills) {
        Strategy strategy;
        switch (typeStrategy) {
            case Default:
                System.out.println("Default");
                strategy = new StrategyDefault();
                return strategy.strategySearch(applicantList, listSkills);
            case DefaultPlus:
                System.out.println("DefaultPlus");
                strategy = new StrategyDefaultPlus();
                return strategy.strategySearch(applicantList, listSkills);

            case Average:
                System.out.println("Average");
                strategy = new StrategyAverage();
                return strategy.strategySearch(applicantList, listSkills);
            case Experience:
                strategy = new StrategyExperience();
                return strategy.strategySearch(applicantList, listSkills);
            case AdvancedSearch:
                strategy = advancedSearchAdaptateur;
                return strategy.strategySearch(applicantList, listSkills);
            default:
                return null;

        }
    }


    public static void setAdvancedSearchAdaptateur(String companyName, String durationExperience) {
        advancedSearchAdaptateur = new AdvancedSearchAdaptateur();
        advancedSearchAdaptateur.setDurationExperience(durationExperience);
        advancedSearchAdaptateur.setCompanyName(companyName);
    }
    

    }
