package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.Experience;

import java.util.ArrayList;
import java.util.List;

public class AdvancedSearch {
    protected AdvancedSearch() {
        System.out.println("creation une instance de search avec AdvancedSearch");
    }

    /**
     *
     * @param listApplicants liste of applicant
     * @param companyName name of company
     * @param durationExperience duration of experience
     * @param listSkills list of skill selected
     * @return list applicant sorted
     */
    public static List<Applicant> searchAdvanced(ApplicantList listApplicants,
                                                 String companyName, String durationExperience,
                                                 List<String> listSkills) {
        Calcul calcul = new CalculAverage();
        List<Applicant> listTrier = new ArrayList<>();
        for (Applicant a : listApplicants) {
            boolean selected = true;
            for (String skillSelect : listSkills) {
                for (String exp : a.getExperiencesKey()) {
                    Experience experience = a.getExperiences().get(exp);
                    List<String> skillsExperience =
                            experience.getKeywords();
                    for (String skillAcquired : skillsExperience) {
                        if ((skillSelect.equals(skillAcquired))) {
                            int difference = (experience.getEndExperience())
                                    - (experience.getStartExperience());
                            if ((!experience.getEntreprise().equals(companyName))
                                    || difference < Integer.parseInt(durationExperience)) {
                                    selected = false;
                                    break;
                            }
                        }
                    }
                }
            }
            if (selected) {
                a.setAverage(calcul.compute(a, listSkills));
                listTrier.add(a);
            }
        }

        Tri.triList(listTrier);
        return new ArrayList<>(listTrier);
    }
}
