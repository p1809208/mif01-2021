package fr.univ_lyon1.info.m1.cv_search.controller;


import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.metier.FactoryStrategies;
import fr.univ_lyon1.info.m1.cv_search.model.metier.TypeStrategy;
import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.view.JfxView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class Controller {


    private final List<JfxView> views;
    private final ApplicantList applicantList;
    private final List<String> skillList;
    private Notify notify;
    private List<Applicant> results;

    /**
     * Controller constructor.
     */
    public Controller() {
        this.views = new ArrayList<>();
        this.applicantList = new ApplicantListBuilder(
                new File(".")).build();
        this.skillList = new ArrayList<>();
    }

    /**
     *
     * @param view instance of view
     */
    public void addView(JfxView view) {
        this.views.add(view);
    }

    /**
     *
     * @param typeStrategy type of strategy
     */
    public void chooseStrategy(TypeStrategy typeStrategy) {
        for (JfxView view : views) {
            if (skillList.isEmpty()) {
                        view.updateResultBoxForErrorUse(Message.NoChoisSkill);
                    } else {
                        run(typeStrategy);
                        assert results != null;
                        view.updateResultBox(results);
                    }
        }

    }

    /**
     *
     * @param typeStrategy type of strategy
     */
    public void run(TypeStrategy typeStrategy) {
        results = FactoryStrategies.makeStrategy(
                typeStrategy, applicantList, skillList);
    }

    /**
     *
     * @param skillName name of skill
     * @param msg message of notification
     */
    public void notifyUpdateSkill(String skillName, Message msg) {
        notify = new MessageSkills(msg, skillName, skillList);
        for (JfxView view : views) {
            notify.notify(view);
        }
    }

    /**
     *
      * @param msg message of notification
     */
    public void notifyUpdateSearch(Message msg) {
        notify = new MessageSearch(msg);
        for (JfxView view : views) {
           notify.notify(view);
        }
    }

    /**
     *
     * @param message message of notification
     */
    public void notifyUpdateAdvancedSearch(Message message) {
        notify = new MessageAdvancedSearch(message);
        for (JfxView view : views) {
            notify.notify(view);
        }
    }

    /**
     *
     * @param companyName name of enterprise
     * @param durationExperience duration of experience
     */
    public void notifySearchWithAdvancedSearch(String companyName, String durationExperience) {
        FactoryStrategies.setAdvancedSearchAdaptateur(companyName, durationExperience);
    }

    /**
     *
     * @param message message of notification
     */
    public void notifyErrorUse(Message message) {
        notify = new MessageError(message);
        for (JfxView view : views) {
            notify.notify(view);

        }
    }

    /**
     *
     * @param value value of strategy
     */
    public void notifyupdateStrategyWidget(String value) {
        for (JfxView view : views) {
            view.updateComboBox(value);

        }
    }

    /**
     * Using in Test.
     * @return skillList
     */
    public List<String> getSkillList() {
        return skillList;
    }

    /**
     *Using Test.
     * @return results
     */

    public List<Applicant> getResults() {
        return results;
    }

}
