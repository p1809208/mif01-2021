package fr.univ_lyon1.info.m1.cv_search.model_test.metier_test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import fr.univ_lyon1.info.m1.cv_search.model.metier.StrategyExperience;
import fr.univ_lyon1.info.m1.cv_search.model.metier.Calcul;
import fr.univ_lyon1.info.m1.cv_search.model.metier.CalculAverage;
import fr.univ_lyon1.info.m1.cv_search.model.metier.Strategy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;

public class StrategyExperienceTest {
    @Test
    public void testStrategyExperience() {
        //Given
        ApplicantListBuilder builderList = new ApplicantListBuilder(new File("./"));
        Strategy strategy = new StrategyExperience();
        Calcul calcul = new CalculAverage();
        List<Applicant>  result = new ArrayList<>();
        //When
        ApplicantList applicants = builderList.build();
        List<String>  list = new ArrayList<>();
        list.add("c");
        list.add("c++");
        list.add("java");
        result = strategy.strategySearch(applicants, list);
        // Then
        assertThat("John Smith", is(result.get(0).getName()));
        assertThat("Tata Tata", is(result.get(1).getName()));
        assertThat("Foo Bar", is(result.get(2).getName()));
        assertThat("Titi Titi", is(result.get(3).getName())); 
        assertThat("Toto Toto", is(result.get(4).getName())); 
        assertEquals(5, result.size());
}
}
