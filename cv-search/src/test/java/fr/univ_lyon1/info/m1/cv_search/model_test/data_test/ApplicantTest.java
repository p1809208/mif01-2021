package fr.univ_lyon1.info.m1.cv_search.model_test.data_test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.data.Experience;
import fr.univ_lyon1.info.m1.cv_search.model.data.ExperienceTab;

public class ApplicantTest {

    @Test
    public void testReadApplicant() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("./applicant1.yaml");
        ApplicantBuilder builder1 = new ApplicantBuilder("./applicant2.yaml");

        // When
        Applicant a = builder.build();
        Applicant a1 = builder1.build();
        //Key and values list of applicant1
        ArrayList<String> key = new ArrayList<>(a.getExperiences().keySet());
        List<Experience> valuesList = new ArrayList<>(a.getExperiences().values());
        //Key and values list of applicant2
        ArrayList<String> key1 = new ArrayList<>(a1.getExperiences().keySet());
        List<Experience> valuesList1 = new ArrayList<>(a1.getExperiences().values());
        List<String> keywordsList = new ArrayList<>();
        List<String> keywordsList1 = new ArrayList<>();
        List<String> keywordsList2 = new ArrayList<>();
        keywordsList.add("c");
        keywordsList.add("c++");
        keywordsList.add("java");
        keywordsList1.add("Python");
        keywordsList1.add("PHP");
        keywordsList1.add("c");
        keywordsList2.add("Scheme");
        keywordsList2.add("c");
        keywordsList2.add("c++");
        keywordsList2.add("java");
        ExperienceTab eT = new ExperienceTab(key.get(0), valuesList.get(0).getStartExperience(), valuesList.get(0).getEndExperience(), valuesList.get(0).getKeywords());

        // Then
        //Test of applicant1
        assertThat(70, is(a.getSkill("c++")));
        assertThat("John Smith", is(a.getName()));
        assertThat(90, is(a.getSkill("c")));
        assertThat(70, is(a.getSkill("c++")));
        assertThat(50, is(a.getSkill("java")));
        assertThat("Google", is(key.get(0)));
        assertThat(2005, is(valuesList.get(0).getStartExperience()));
        assertThat(2010, is(valuesList.get(0).getEndExperience()));
        assertThat(keywordsList, is(valuesList.get(0).getKeywords()));
        assertEquals(5, (valuesList.get(0).getEndExperience() - valuesList.get(0).getStartExperience()));
        assertThat("Facebook", is(key.get(1)));
        assertThat(2010, is(valuesList.get(1).getStartExperience()));
        assertThat(2021, is(valuesList.get(1).getEndExperience()));
        assertThat(keywordsList1, is(valuesList.get(1).getKeywords()));
        assertEquals(11, (valuesList.get(1).getEndExperience() - valuesList.get(1).getStartExperience()));
        //Test of applicant2
        assertThat(70, is(a1.getSkill("c++")));
        assertThat("Foo Bar", is(a1.getName()));
        assertThat(50, is(a1.getSkill("c")));
        assertThat(70, is(a1.getSkill("c++")));
        assertThat(50, is(a1.getSkill("java")));
        assertThat("UCBL", is(key1.get(0)));
        assertThat(2004, is(valuesList1.get(0).getStartExperience()));
        assertThat(2021, is(valuesList1.get(0).getEndExperience()));
        assertThat(keywordsList2, is(valuesList1.get(0).getKeywords()));
        assertEquals(17, (valuesList1.get(0).getEndExperience() - valuesList1.get(0).getStartExperience()));
        //Test of ExperienceTab
        assertThat("Google", is(eT.getEntreprise()));
        assertThat(2005, is(eT.getStartExperience()));
        assertThat(2010, is(eT.getEndExperience()));
        assertThat(keywordsList, is(eT.getKeywords()));
    }

    @Test
    public void testReadManyApplicant() {
        // Given
        ApplicantListBuilder builderList = new ApplicantListBuilder(new File("."));
        // When
        ApplicantList list = builderList.build();

        // Then
        boolean johnFound = false;
        for (Applicant a : list) {
            if (a.getName().equals("John Smith")) {
                assertThat(90, is(a.getSkill("c")));
                assertThat(70, is(a.getSkill("c++")));
                assertThat(50, is(a.getSkill("java")));
                
                johnFound = true;
            }
        }
        assertThat(johnFound, is(true));

         boolean FooFound = false;
        for (Applicant a1 : list) {
            if (a1.getName().equals("Foo Bar")) {
                assertThat(50, is(a1.getSkill("c")));
                assertThat(70, is(a1.getSkill("c++")));
                assertThat(50, is(a1.getSkill("java")));
                FooFound = true;
            }
        }
        assertThat(FooFound, is(true)); 
    } 
}