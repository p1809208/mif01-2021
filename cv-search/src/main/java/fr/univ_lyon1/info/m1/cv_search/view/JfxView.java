package fr.univ_lyon1.info.m1.cv_search.view;



import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.controller.Message;
import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.data.ApplicantTab;
import fr.univ_lyon1.info.m1.cv_search.model.data.ExperienceTab;
import fr.univ_lyon1.info.m1.cv_search.model.metier.TypeStrategy;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.geometry.Pos;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class JfxView {
    private HBox searchSkillsBox;
    private VBox resultBox;
    private VBox advancedSearchBox;
    private Label labelErrorCompany;
    private Label labelErrorDuration;
    private TableView<ApplicantTab> table;
    private ComboBox<String> comboBox = new ComboBox<String>();
    private Controller controller;

    /**
     * Create the main view of the application.
     */
    public JfxView(Controller c, Stage stage, int width, int height) {
        // Name of window
        stage.setTitle("Search for CV");
        VBox root = new VBox();
        root.setPadding(new Insets(5, 100, 50, 80));
        root.setSpacing(20);
        Image image = new Image("file:select-cv.png");
        ImageView imageView = new ImageView(image);
        imageView.setImage(image);
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(200);
        imageView.setFitHeight(200);
        root.getChildren().add(imageView);
        Node newSkillBox = createNewSkillWidget();
        root.getChildren().add(newSkillBox);
        Node searchSkillsBox = createCurrentSearchSkillsWidget();
        root.getChildren().add(searchSkillsBox);
        Node strategyBox = createStrategyWidget();
        root.getChildren().add(strategyBox);
        Node search = createSearchWidget();
        root.getChildren().add(search);
        Node advancedSearchBox = createAdvancedSearchWidget();
        root.getChildren().add(advancedSearchBox);
        Node resultBox = createResultsWidget();
        root.getChildren().add(resultBox);
        root.setStyle("-fx-background-color: #e8e4d9;"); 
        // Everything's ready: add it to the scene and display it
        Scene scene = new Scene(root, width, height);

        stage.setScene(scene);
        stage.show();
        this.controller = c;
    }

    private Node createCurrentSearchSkillsWidget() {
        searchSkillsBox = new HBox();
        return searchSkillsBox;
    }

    /**
     * Create the text field to enter a new skill.
     */
    private Node createNewSkillWidget() {
        HBox newSkillBox = new HBox();
        Label labelSkill = new Label("Skill:");
        TextField textField = new TextField();
        Button submitButton = new Button("Add skill");
        newSkillBox.getChildren().addAll(labelSkill, textField, submitButton);
        newSkillBox.setSpacing(10);

        EventHandler<ActionEvent> skillHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text = textField.getText().strip();
                if (text.equals("")) {
                    return; // Do nothing
                }
                if (text.equals("PHP") || text.equals("Python") || text.equals("Scheme")) {
                    controller.notifyUpdateSkill(text, Message.AddSkill);
                } else {
                    controller.notifyUpdateSkill(text.toLowerCase(Locale.ROOT), Message.AddSkill);
                }
                    textField.setText("");
                    textField.requestFocus();

            }
        };
        submitButton.setOnAction(skillHandler);
        textField.setOnAction(skillHandler);
        return newSkillBox;
    }


    public void updateButtonSkill(String nameSkill, Message message) {
            switch (message) {
                case AddSkill:
                    updateAddButtonSkill(nameSkill);
                    break;
                case RemoveSkill:
                    updateRemoveButtonSkill(nameSkill);
                    break;
                default:
            }
        }
    private void updateAddButtonSkill(String nameSkill) {
        HBox box = new HBox();
        Text text = new Text(nameSkill);
        Button b = new Button("x");
        box.setStyle("-fx-padding: 2;" + "-fx-border-style: solid inside;"
                + "-fx-border-width: 1;" + "-fx-border-insets: 5;"
                + "-fx-border-radius: 5;" + "-fx-border-color: black;");
        box.setAlignment(Pos.BASELINE_CENTER);
        box.getChildren().addAll(text, b);
        searchSkillsBox.getChildren().add(box);
        b.setOnAction(event -> {
            controller.notifyUpdateSkill(nameSkill, Message.RemoveSkill);
        });
    }
    private void updateRemoveButtonSkill(String nameSkill) {
        for (int i = 0; i < searchSkillsBox.getChildren().size(); i++) {
            Node skill = searchSkillsBox.getChildren().get(i);
            Text skillName = (Text) ((HBox) skill).getChildren().get(0);
            String s = skillName.getText();
            if (s.equals(nameSkill)) {
                searchSkillsBox.getChildren().remove(skill);
            }
        }
    }




    /**
     * Create the widget showing the list of applicants.
     */
    private Node createResultsWidget() {
        resultBox = new VBox();
        return resultBox;
    }

    /**
     * Create the widget used to trigger the search.
     */
    private Node createSearchWidget() {
        Button search = new Button("Search");
        search.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (resultBox.getChildren().size() > 1) {
                    return;
                }
                if (comboBox.getValue() == null) {
                    controller.notifyErrorUse(Message.NoSelectStrategy);

                } else {
                    controller.chooseStrategy(TypeStrategy.getStrategy(comboBox.getValue()));
                    controller.notifyUpdateSearch(Message.AddResetButton);

                }
            }
        });

        return search;
    }





    /**
     * Create the widget showing the list of skills currently searched
     * for.
     */


    private Node createStrategyWidget() {
        HBox newStrategyBox = new HBox();
        Label labelStrategy = new Label("Strategy : ");
        comboBox.getItems().addAll(TypeStrategy.Default.valeur(),
                TypeStrategy.DefaultPlus.valeur(),
                TypeStrategy.Average.valeur(),
                TypeStrategy.Experience.valeur());
        newStrategyBox.getChildren().addAll(labelStrategy, comboBox);
        newStrategyBox.setSpacing(10);
        comboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controller.notifyupdateStrategyWidget(comboBox.getValue());
            } });
        return newStrategyBox;
    }

    public void updateComboBox(String value) {
        comboBox.setValue(value);
    }


    public void updateResultBox(List<Applicant> applicants) {
        table = new TableView<ApplicantTab>();
        final Label label = new Label("Search result");
        label.setFont(new Font("Arial", 20));
        for (Applicant applicant : applicants) {
            createResultsTable(applicant);
        }
        resultBox.getChildren().addAll(label, table);
    }


    public void createResultsTable(Applicant applicant) {
        TableColumn<ApplicantTab, String> applicantNameCol =
                new TableColumn<ApplicantTab, String>("Name");
        TableColumn<ApplicantTab, Integer> averageCol =
                new TableColumn<ApplicantTab, Integer>("Average");
        TableColumn<ApplicantTab, List<ExperienceTab>> applicantExpersCol =
                new TableColumn<ApplicantTab, List<ExperienceTab>>("Experiences");
        applicantNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        averageCol.setCellValueFactory(new PropertyValueFactory<>("average"));
        applicantExpersCol.setCellValueFactory(new PropertyValueFactory<>("listExperiences"));
        List<ExperienceTab> experienceTabList = new ArrayList<>();
        table.getColumns().clear();
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        for (String exp : applicant.getExperiences().keySet()) {
            ExperienceTab experienceTab = new ExperienceTab(
                    applicant.getExperience(exp).getEntreprise(),
                    applicant.getExperience(exp).getStartExperience(),
                    applicant.getExperience(exp).getEndExperience(),
                    applicant.getExperience(exp).getKeywords());
            experienceTabList.add(experienceTab);
            }

        ApplicantTab aTab = new ApplicantTab(applicant.getName(),
                applicant.getAverage(), experienceTabList);
        table.getColumns().addAll(applicantNameCol, averageCol, applicantExpersCol);
        table.getItems().add(aTab);
        resultBox.setSpacing(5);
        resultBox.setPadding(new Insets(10, 0, 0, 10));
    }

    public void updateSearch(Message message) {
        switch (message) {
            case AddResetButton:
                addResetButton();
                break;
            case ResetSearch:
                resultBox.getChildren().clear();
                break;
            default:
        }

    }

    private void addResetButton() {
        Button removeBtn = new Button("Search initialization");
        removeBtn.setStyle("-fx-font: 13 arial; "
                + "-fx-base: #563913; "
                + "-fx-highlight-text-fill: white");
        resultBox.getChildren().add(removeBtn);
        removeBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controller.notifyUpdateSearch(Message.ResetSearch);
            }
        });
    }

    private Node createAdvancedSearchWidget() {
        advancedSearchBox = new VBox();
        Button advancedSearchBtn = new Button("Advanced Search");
        advancedSearchBtn.setStyle("-fx-font: 13 arial; -fx-base: #563913; "
                + "-fx-highlight-text-fill: white");
        advancedSearchBox.getChildren().add(advancedSearchBtn);
        advancedSearchBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (advancedSearchBox.getChildren().size() > 1) {
                    return;
                }
                controller.notifyUpdateAdvancedSearch(Message.ActivateAdvancedSearch);
            }
        });
        return advancedSearchBox;
    }



    public void updateAdvancedSearch(Message message) {
        switch (message) {
            case ActivateAdvancedSearch:
                updateActivateAdvancedSearch();
                break;
            case DisableAdvancedSearch:
                updateDisableAdvancedSearch();
                break;
            default:

        }
    }
    private void updateActivateAdvancedSearch() {
        HBox hBox1 = new HBox();
        HBox hBox2 = new HBox();
        Label label1 = new Label("Company:");
        label1.setTextFill(Color.web("#666666"));
        TextField textField = new TextField();
        hBox1.getChildren().addAll(label1, textField);
        Label label2 = new Label("Duration:");
        label2.setTextFill(Color.web("#666666"));
        TextField textField2 = new TextField();
        hBox2.getChildren().addAll(label2, textField2);
        Button applyBtn = new Button("Apply");
        Button cancelBtn = new Button("Cancel");
        advancedSearchBox.getChildren().addAll(hBox1, hBox2, applyBtn, cancelBtn);
        cancelBtn.setOnAction(event -> {
            controller.notifyUpdateAdvancedSearch(Message.DisableAdvancedSearch);
        });
        applyBtn.setOnAction(event -> {
            if (resultBox.getChildren().size() > 1) {
                return;
            }
            if (textField.getText().equals("")) {
                controller.notifyErrorUse(Message.deleteMsgErrorDuration);
                controller.notifyErrorUse(Message.NoChoisCompany);
            } else if (textField2.getText().equals("")) {
                controller.notifyErrorUse(Message.deleteMsgErrorCompany);
                controller.notifyErrorUse(Message.NoChoisDuration);
            } else {
                controller.notifyErrorUse(Message.deleteMsgErrorCompany);
                controller.notifyErrorUse(Message.deleteMsgErrorDuration);
                controller.notifySearchWithAdvancedSearch(
                        textField.getText(), textField2.getText());
                controller.chooseStrategy(TypeStrategy.AdvancedSearch);
                controller.notifyUpdateSearch(Message.AddResetButton);
            }


        });
    }

    private void updateDisableAdvancedSearch() {
        while (advancedSearchBox.getChildren().size() != 1) {
            Node champ = advancedSearchBox.getChildren()
                    .get(advancedSearchBox.getChildren().size() - 1);
            advancedSearchBox.getChildren().remove(champ);
        }
    }
    public void updateResultBoxForErrorUse(Message message) {
        switch (message) {
            case NoSelectStrategy:
                resultBox.getChildren().add(new Label("Be sure to choose a strategy"));
                break;
            case NoChoisSkill:
                resultBox.getChildren().add(new Label("Be sure to choose a skill"));
                break;
            case NoChoisCompany:
                if (labelErrorCompany == null) {
                    labelErrorCompany = new Label("Be sure to choose a Company name");
                    advancedSearchBox.getChildren().add(labelErrorCompany);
                }
                break;
            case NoChoisDuration:
                if (labelErrorDuration == null) {
                    labelErrorDuration = new Label("Be sure to choose a duration period");
                    advancedSearchBox.getChildren().add(labelErrorDuration);
                }
                break;
            case deleteMsgErrorCompany:
                advancedSearchBox.getChildren().remove(labelErrorCompany);
                labelErrorCompany = null;
                break;
            case deleteMsgErrorDuration:
                advancedSearchBox.getChildren().remove(labelErrorDuration);
                labelErrorDuration = null;
                break;
            default:
        }
    }

}

