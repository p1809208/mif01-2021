package fr.univ_lyon1.info.m1.cv_search.model.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class ApplicantBuilder {

    private File file;

    public ApplicantBuilder(File f) {
        this.file = f;
    }

    public ApplicantBuilder(String filename) {
        this.file = new File(filename);
    }

    /**
     * Build the applicant from the Yaml file provided to the constructor.
     */
    public Applicant build() {
        Applicant a = new Applicant();
        /* Experience exp = new Experience(); */
        Yaml yaml = new Yaml();
        Map<String, Object> map;
        
        try {
            map = yaml.load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new Error(e);
        }

        a.setName((String) map.get("name"));
        // Cast may fail if the Yaml is incorrect. Ideally we should provide
        // clean error messages.
        @SuppressWarnings("unchecked")
        Map<String, Integer> skills = (Map<String, Integer>) map.get("skills");
        // Cast may fail if the Yaml is incorrect. Ideally we should provide
        // clean error messages.
        @SuppressWarnings("unchecked")
        Map<String, Object> experiences = (Map<String, Object>) map.get("experience");
        for (String skill : skills.keySet()) {
            Integer value = skills.get(skill);
            a.setSkill(skill, value);
        }
        if (experiences != null) {
            for (String experience : experiences.keySet()) {
                @SuppressWarnings("unchecked")
                Map<String, Object> value = (Map<String, Object>) experiences.get(experience);
                Integer start = (Integer) value.get("start");
                Integer end = (Integer) value.get("end");
                @SuppressWarnings("unchecked")
                List<String> keyword = (List<String>) value.get("keywords");
                a.setExperiences(experience, new Experience(experience, start, end, keyword));

            }
        } else {
            a.setExperiences(null,
                    new Experience("no experience found ",
                            0, 0, null));
        }

        return a;
    }
}
