package fr.univ_lyon1.info.m1.cv_search.model.metier;

import fr.univ_lyon1.info.m1.cv_search.model.data.Applicant;

import java.util.List;

public class CalculAverage implements Calcul {
    public CalculAverage() {
        System.out.println("Creation une instance de CalculAverage");
    }
    @Override
    public int compute(Applicant applicant, List<String> listSkills) {
        int average = 0;
        int skillSelect = 0;
        int scoreSkill = 0;
        for (String skill : listSkills) {
            skillSelect += 1;
            scoreSkill += applicant.getSkill(skill);
            average = scoreSkill / skillSelect;
        }
        return average;
    }
}
